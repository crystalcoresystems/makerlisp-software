; Load a file into a 2d vector with the end of file at start of vector
(include '<floadfile.l>)

(define load-as-2d-vector (lambda (fnam)
(define f (fopen fnam 'r))
(if (zerop f)
  (error '"fnf"))
(define vector2d)
(setq vector2d (list2vector (cons (vector) (reverse (floadfile f))) ))
(fclose f)
vector2d
))
