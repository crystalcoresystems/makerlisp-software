(define floadline (lambda (f c)


(if (or (= c 13) (= c -1) (= c 10))
  (list)
  (cons c (floadline f (fgetc f)))
)
))
