(include '/bin/vbuf2/vsplit.l)

(define vbsplaft (lambda (b x) (vsplit b (- (vlen b) (- x 1)))))