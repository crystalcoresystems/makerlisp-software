; Concatenate two vectors together at index x
(include '/bin/vbuf2/vbsplaft.l)

(define vbcats (lambda (b x)
(define slist (vbsplaft b x))
(define cat-to (car slist))
(define cat-from (cadr slist))
(define end (- (vlen cat-to) 1))
(vset cat-to end (list2vector 
  (append (vector2list (vref cat-to end)) (vector2list (vref cat-from 0)))))
(list2vector (append (vector2list cat-to) (cdr (vector2list cat-from))))
))
