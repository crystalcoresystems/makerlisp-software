; load given file into a list of reversed vectors
(include '<floadline.l>)

(define floadfile (lambda (f)
(define line)
(define c)
(if (not (= (setq c (fgetc f)) -1))
  (progn
  (setq line (list2vector (reverse (floadline f c))))
  (cons line (floadfile f))
))
))
