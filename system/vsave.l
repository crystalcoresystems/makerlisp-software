;; Idea - takes a filename and finds highest version token in current directory
;;        moves the current file to that version plus 1
;;

(define vsave
  (lambda args
    ; find the maximum version # of a given file in current directory
    (define max-version
      (lambda (string)
         (define fname)
         (define ver)
         (define max 0)
         (opendir)
         (while 't
           (setq fname (lsitem))
           (if (eq fname '"")
             (break))
           (setq ver (get-version fname string))
           (if (> ver max)
             (setq max ver)))
           (closedir)
          max))

    ; parse filename for the version number
    (define get-version
      (lambda (fname string)
        (define tokens)
        (setq tokens (parse fname '";"))
        (if (eq (car tokens) string)
          (if (cadr tokens) (atoi (cadr tokens)) 0)
          -1
        )
     ))


    (define _vsave
      (lambda (k)
        (global "*error-continue*" k)
        (define fname (car args))
        (define x (max-version fname))
        (define newname (cats fname (cats '";" (itoa (+ x 1)))))
        (move fname newname)
    '""))

    ; Save old exception handler
    (define old-error-handler
      (if (assignedp '"*error-continue*") "*error-continue*"))

    ; Call vsave function
    (define r (call/cc _vsave))

    ; Exceptions come here, that's the only way out of _vsave
    (global "*error-continue*" old-error-handler)

    (if (not (eq r '"")) (nlprint r))))





