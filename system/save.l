(define save (lambda (string)
(define cmds (history))
(define cmd)
(define parsedcmd)
(define c)

(while (> (length cmds) 0)
  (setq cmd (car cmds))
  (setq parsedcmd (parse cmd '" "))
  (if (and (eq (car parsedcmd) '"(define")
           (eq (cadr parsedcmd) string)  )
    (progn

    (define f (fopen (cats '"/bin/scripts/" (cats string '".l")) 'w))
    (while (> (strlen cmd) 0)
       (fputc (sym2int (car cmd)) f)
       (setq cmd (cdr cmd)))
    (fclose f)
    (setq cmds (list))
 ))


(setq cmds (cdr cmds)))
))
