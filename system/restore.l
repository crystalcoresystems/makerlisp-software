(define _restore (lambda (name number)
  (nlprint '"restoring")
  (copy name 'temp)
  (move (cats name (cats '";" number)) name)
  (move 'temp (cats name (cats '";" number)))
))
; Restore categorized object
(define restore
  (lambda args
    (define name (car args))
    (define number (cadr args))





    (opendir)
    (define tokens)
    (define fname)
    (while 't
      (setq fname (lsitem))
      (if (eq fname '"")
        (break))
      (setq tokens (parse fname '";"))

      (if (and (cadr tokens) (eql number (atoi (cadr tokens))))
        (_restore name (cadr tokens))))
    (closedir)
    '""))
