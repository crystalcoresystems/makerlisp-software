; Finite State machine with escape and insert modes

; Input: b -> Variable buffer with the first 28 lines of file
;       ib -> Buffer containing the rest of the file

; Output: b -> Altered variable buffer
;        ib -> Buffer containing portion of the file not loaded

; Idea: Escape mode - hjkl to navigate file, i to enter insert mode
;                     When scrolling past the bottom of the screen
;                     load the next line of the file from ib and
;                     redraw the screen

;       Insert mode - Entering insert mode splits the current line
;                     buffer unless cursor is at top of buffer stack
;                     (end of line)
;                     Enter creates a new line in the buffer and
;                           redraws the screen
;                     Delete removes a character and redraws the line
;                     Esc enters escape mode and if the buffer was
;                         split concatenates the line together
;                     Otherwise characters are added to the buffer
;                     as they are typed.

(include '<vbadd.l>)
(include '<vbsub.l>)
(include '<vbsplit.l>)
(include '<vbcats.l>)
(include '<mv-cur.l>)
(include '<redraw.l>)


(define writer (lambda (b)

(define flag 0) ; Flag is 1 if the buffer is split
(define mode 1) ; Mode 1 indicates Escape mode, 0 indicates insert
(define cx 1)   ; Keeps track of x position of cursor in buffer
(define cy 1)   ; Keeps track of y position of cursor in buffer
(define c)
(define d)
(define i)
(define elem)
(define screen_location 1)

(while (not (eql (setq c (getchar)) 26))
  (cond
    ((eql c 27)
      (setq mode 1)
      (if (eql flag 1) (progn
        (setq b (vbcats b cx))
        (setq flag 0)
      ))
    )
    ((eql mode 0)
      (cond
        ((eql c 13))
        ((eql c 10)
          (setq cx (+ cx 1))
          (setq b (vbsplit b cx 1))
          (setq cy 1)
          (if (< (- cx screen_location) 28)
          (redraw b (- (+ cx 1) screen_location) (+ (- (vlen b) screen_location) 1) cy flag)
          (progn (setq screen_location (+ 1 screen_location))     
          (redraw b (- (+ cx 1) screen_location) (+ (- (vlen b) screen_location) 1) cy flag)))
       )

        ((eql c 8)
          (vbsub b cx)
          (setq cy (- cy 1))
          (putchar c)
          (if (eql flag 1) (progn
            ; hide cursor
            (setq d (defer 't))
            (print '"\e[?25l")
            ; put all characters in the next buffer
            ; starting with the last (- (vlen b) 1)
            (setq i 1)
            (setq elem (vref b (- (vlen b) (+ 1 cx))))
            (while (>= (- (vlen elem) i) 0)
              (putchar (vref elem (- (vlen elem) i)))
              (setq i (+ i 1)))
            ; move cursor back
            (putchar 46)
            (print '"\e[")
            (print cy)
            (print '"G")
            ;show cursor
           (print '"\e[?25h")
           (defer d)
          )(progn (putchar 46)(putchar c)))
        )
        ('t
          (putchar c)
          (vbadd b cx c)
          (setq cy (+ cy 1))
          (if (eql flag 1) (progn
            ; hide cursor
            (setq d (defer 't))
            (print '"\e[?25l")
            ; put all characters in the next buffer
            ; starting with the last (- (vlen b) 1)
            (setq i 1)
            (setq elem (vref b (- (vlen b) (+ 1 cx))))
            (while (>= (- (vlen elem) i) 0)
              (putchar (vref elem (- (vlen elem) i)))
              (setq i (+ i 1)))
            ; move cursor back
            (print '"\e[")
            (print cy)
            (print '"G")
            ;show cursor
           (print '"\e[?25h")
           (defer d)
          ))
        )
      )
    )
    ((eql mode 1)
      (cond
        ((eql c 105)
          (setq mode 0)
          (cond  
            ( (= cy 1) (setq flag 1) )
            ( (<= cy (vlen (vref b (- (vlen b) cx))))
            (setq flag 1) )
          )
          (setq b (vbsplit b cx cy))
        )
        ((eql c 104)
          (if (> cy 1) (progn
            (setq cy (- cy 1))
            (mv-cur (int2sym c))
          ))        )
        ((eql c 106)
          (if (< (- cx screen_location) 27)
            (if (< cx (- (vlen b) 1) )
            (progn
              (mv-cur (int2sym c))
              (setq cx (+ cx 1)) ))
            (progn
              (if (< cx (- (vlen b) 1) )
               (progn
                (setq cx (+ cx 1))
                (setq screen_location (+ screen_location 1))
                (redraw b 28 (+ (- (vlen b) screen_location) 1) cy flag)
                ))
             ))
        )
        ((eql c 107)
          (if (> cx 1) (progn
            (mv-cur (int2sym c))
            (setq cx (- cx 1))
            (if (< cx screen_location) (progn
              (setq screen_location (- screen_location 1))
              (redraw b 1 (+ (- (vlen b) screen_location) 1) cy flag)
            ))
          ))
        )
        ((eql c 108)
          (if (<= cx (vlen b))
            (if (<= cy (vlen (vref b (- (vlen b) cx))))
              (progn
                (mv-cur (int2sym c))
                (setq cy (+ cy 1))
          )))
        )
      )
    )
  )
)
(if (eql flag 1)
  (setq b (vbcats b cx))
)
b
))
