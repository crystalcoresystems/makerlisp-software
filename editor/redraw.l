; Redraw the screen
(define redraw
      (lambda (vb loc screen_location cy flag)

        (define c)
        (define d)
        (define i)
        (define j)
        (define line)
        ; Turn cursor off
        (setq d (defer 't))
        (print '"\e[?25l")

        ; 28 lines of 80 characters
        (setq j 1)
        ; Stop redraw before 28 if the buffer is smaller
        (while (and (< j 29) (< j (vlen vb)) )
          (print '"\e[")
          (print j)
          (print '";1H")
          (setq i 1)
          ; Avoid redrawing split buffer if flag was passed to procedure
          (if (= flag 1)
            (if (>= j loc)
              (setq line (- (- screen_location j) 1))
              (setq line (- screen_location j))
            )
            (setq line (- screen_location j))
          )
          (while (< i 81)
             (if (<= i (vlen (vref vb line)))
               (putchar (vref (vref vb line) (- (vlen (vref vb line)) i )))
               (print '"."))
             (setq i (+ i 1)))
          (setq j (+ j 1)))
         (print '"\e[")
         (print loc)
         (print '";")
         (print cy)
         (print '"H")

        ; Turn cursor back on
        (print '"\e[?25h")
        (defer d)
        ))
