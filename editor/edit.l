; Simple editor - just a start at the moment

(include '<variable-buffer-procedures.l>)
(include '<writer.l>)
(include '<file-load-procedures.l>)
(include '<vsave.l>)


(define edit
  (lambda args

    ; Draw the frame
    (define frame
      (lambda ()

        (define d)
        (define i)
        (define j)

        ; Turn cursor off
        (setq d (defer 't))
        (print '"\e[?25l")

        ; Clear screen
        (cls)

        ; Mode line
        (print '"\e[29;1H")
        (setq i 1)
        (while (< i 81)
          (putchar 196)
          (setq i (+ i 1)))

        ; Restore cursor
        (print '"\e[H")

        ; Turn cursor back on
        (print '"\e[?25h")
        (defer d)))

    ; Fill buffer display area with text
    (define text
      (lambda (vb)

        (define c)
        (define d)
        (define i)
        (define j)
        (define flag)


        ; Turn cursor off
        (setq d (defer 't))
        (print '"\e[?25l")

        ; 28 lines of 80 characters
        (setq j 1)
        (while (< j 29)
          (print '"\e[")
          (print j)
          (print '";1H")
          (setq i 1)

          (while (< i 81)
             (if (> (vlen vb) j)
               (if (<= i (vlen (vref vb (- (vlen vb) j)) ))
                 (putchar (vbget vb j i))
                 (print '"."))
               (print '"."))
             (setq i (+ i 1)))
          (setq j (+ j 1)))
        (print '"\e[H")

        ; Turn cursor back on
        (print '"\e[?25h")
        (defer d)
        ))

    (define _edit
      (lambda (k)

      ; Set exception handler
      (define df (defer 't))
      (global "*error-continue*" k)
      (defer df)
      
      ; Turn off line wrap
      (print '"\e[?7l")

      ; Load buffer
      (define vb (load-as-2d-vector (car args)))
      (if (= (vlen vb) 1) (setq vb (vector (vector) (vector))) )


      ; Draw frame
      (frame)

      ; Fill buffer display area with text
      (text vb)

      ; Wait
      (setq vb (writer vb))

      ; Save file
      (define fname (car args))
      (vsave fname)
      (define f (fopen fname 'w))
      (define i (- (vlen vb) 1))
      (while (> i 0)
        (define j (- (vlen (vref vb i)) 1))
        (while (>= j 0)
          (fputc (vref (vref vb i) j) f)
          (setq j (- j 1)))
        (fputc 10 f)
        (setq i (- i 1)))
      (fclose f)

      '""))

    ; Start here

    ; Save old exception handler
    (define old-error-handler
      (if (assignedp '"*error-continue*") "*error-continue*"))

    ; Call edit function
    (define r (call/cc _edit))

    ; Exceptions come here, that's the only way out of _edit

    ; Restore normal terminal state and previous exception handler
    (define df (defer 't))
    (print '"\e[?7h")
    (print '"\e[?25h")
    (global "*error-continue*" old-error-handler)
    (defer df)

    (cls)

    (if (not (eq r '"")) (nlprint r))))
